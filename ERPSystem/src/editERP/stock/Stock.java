package editERP.stock;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Vector;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import editERP.JTable2Pdf;

public class Stock extends JFrame {

	private JPanel contentPane;
	private JTextField tfSearch;
	private JTable table;
	
	private SortlistDAO sortlistDao;
	private CompanyDAO companyDao;
	private ItemsDAO itemsDao;
	private Vector cols;
	private DefaultTableModel model;
	private JButton btnEdit;
	private JButton btnUpdate;
	private String lv;

	/**
	 * Launch the application.
	 */
//	public static void main(String[] args) {
//		EventQueue.invokeLater(new Runnable() {
//			public void run() {
//				try {
//					Stock frame = new Stock();
//					frame.setVisible(true);
//				} catch (Exception e) {
//					e.printStackTrace();
//				}
//			}
//		});
//	}

	/**
	 * Create the frame.
	 * @param lv2 
	 */

	
	public Stock(String lv2) {
		this.lv = lv2;
		cols=new Vector();
		cols.add("분류");
		cols.add("제조사");
		cols.add("제품코드");
		cols.add("제품명");
		cols.add("중량");
		cols.add("재고");
		itemsDao=new ItemsDAO();
		
		setTitle("재고현황");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 1000, 850);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		JPanel panel = new JPanel();
		contentPane.add(panel, BorderLayout.NORTH);
		
		JLabel lblNewLabel = new JLabel("제품명 : ");
		
		tfSearch = new JTextField();
		tfSearch.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent e) {
			search();
			}
		});
		tfSearch.setColumns(10);
		
		JButton btnNewButton = new JButton("검색");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			search();
			}
		});
		
		btnUpdate = new JButton("추가");
		if (lv2.equals("관리자")){
			btnUpdate.setEnabled(true);
		}else if(lv2.equals("일반")){
			btnUpdate.setEnabled(false);
		}
		btnUpdate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(lv2.equals("관리자")){
					StockSave frm=new StockSave(Stock.this);
					frm.setVisible(true);
					frm.setLocation(200, 200);					
				}else{
					JOptionPane.showMessageDialog(Stock.this, "권한이 없습니다.");
				}
			}
		});
		
		btnEdit = new JButton("수정");
		if (lv2.equals("관리자")){
			btnEdit.setEnabled(true);
		}else if(lv2.equals("일반")){
			btnEdit.setEnabled(false);
		}
		btnEdit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(lv2.equals("관리자")){
					int idx=table.getSelectedRow();
					if(idx==-1){
						JOptionPane.showMessageDialog(Stock.this, "편집할 자료를 선택하세요.");
						return;
					}else{
						String sortname=table.getValueAt(idx, 0)+"";
						String compname=table.getValueAt(idx, 1)+"";
						int itemcode=Integer.valueOf(table.getValueAt(idx, 2)+"");
						String itemname=table.getValueAt(idx, 3)+"";		
						String weight=table.getValueAt(idx, 4)+"";
						int itemstock=Integer.valueOf(table.getValueAt(idx, 5)+"");
						StockDTO dto=new StockDTO(sortname, compname, itemcode, itemname, weight, itemstock);
						StockEdit frm=new StockEdit(Stock.this, dto);
						frm.setVisible(true);
					}
				}else{
					JOptionPane.showMessageDialog(Stock.this, "권한이 없습니다.");
				}
			}
		});
		
		JButton btnNewButton_1 = new JButton("출력");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JTable2Pdf pdf = new JTable2Pdf(table);
				pdf.dispose();
				search();
			}
		});
		GroupLayout gl_panel = new GroupLayout(panel);
		gl_panel.setHorizontalGroup(
			gl_panel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel.createSequentialGroup()
					.addGap(61)
					.addComponent(lblNewLabel)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(tfSearch, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
					.addGap(4)
					.addComponent(btnNewButton)
					.addGap(438)
					.addComponent(btnUpdate)
					.addGap(5)
					.addComponent(btnEdit)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(btnNewButton_1, GroupLayout.PREFERRED_SIZE, 65, GroupLayout.PREFERRED_SIZE)
					.addContainerGap(54, Short.MAX_VALUE))
		);
		gl_panel.setVerticalGroup(
			gl_panel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel.createSequentialGroup()
					.addGap(5)
					.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_panel.createParallelGroup(Alignment.BASELINE)
							.addComponent(btnEdit)
							.addComponent(btnNewButton_1))
						.addGroup(gl_panel.createSequentialGroup()
							.addGap(4)
							.addComponent(lblNewLabel))
						.addGroup(gl_panel.createParallelGroup(Alignment.BASELINE)
							.addComponent(btnNewButton)
							.addComponent(tfSearch, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
						.addComponent(btnUpdate))
					.addContainerGap())
		);
		panel.setLayout(gl_panel);
		
		JScrollPane scrollPane = new JScrollPane();
		contentPane.add(scrollPane, BorderLayout.CENTER);
		
		table = new JTable();
		table.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
//				StockSave frm=new StockSave(Stock.this);
//				frm.setVisible(true);
//				frm.setLocation(200, 200);
			}
		});
		scrollPane.setViewportView(table);
		refreshModel();
	}
	public void refreshModel() {
		model=new DefaultTableModel(itemsDao.listItems(), cols) {
			@Override
		public boolean isCellEditable(int row, int column) {
			return false;
		}
	};
		table.setModel(model);
	}
	public void search() {
		String itemname=tfSearch.getText();
		model=new DefaultTableModel(itemsDao.searchItems(itemname), cols) {
			@Override
		public boolean isCellEditable(int row, int column) {
			return false;
		}
	};
	table.setModel(model);
	}
}
