package editERP.stock;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import editERP.DB;

public class SortlistDAO {
public ArrayList<SortlistDTO> listSortlist(){
	ArrayList<SortlistDTO> items=new ArrayList<>();
	Connection conn=null;
			PreparedStatement pstmt=null;
			ResultSet rs=null;
			try {
				conn=DB.oraConn();
				String sql="select * from sortlist order by sortcode";
				pstmt=conn.prepareStatement(sql);
				rs=pstmt.executeQuery();
				while(rs.next()){
					int sortcode=rs.getInt("sortcode");
					String sortname=rs.getString("sortname");
					SortlistDTO dto=new SortlistDTO(sortcode, sortname);
					items.add(dto);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}finally{
				try {
					if(rs!=null)rs.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				try {
					if(pstmt!=null)pstmt.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				try {
					if(conn!=null)conn.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			return items;
}
}
