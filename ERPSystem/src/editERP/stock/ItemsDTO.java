package editERP.stock;

public class ItemsDTO {
public int sortcode;
public int compcode;
public int itemcode;
public String itemname;
public String weight;
public int itemstock;
public int itemprice;
public ItemsDTO() {
	// TODO Auto-generated constructor stub
}
public ItemsDTO(int sortcode, int compcode, int itemcode, String itemname, String weight, int itemstock,
		int itemprice) {
	super();
	this.sortcode = sortcode;
	this.compcode = compcode;
	this.itemcode = itemcode;
	this.itemname = itemname;
	this.weight = weight;
	this.itemstock = itemstock;
	this.itemprice = itemprice;
}
public int getSortcode() {
	return sortcode;
}
public void setSortcode(int sortcode) {
	this.sortcode = sortcode;
}
public int getCompcode() {
	return compcode;
}
public void setCompcode(int compcode) {
	this.compcode = compcode;
}
public int getItemcode() {
	return itemcode;
}
public void setItemcode(int itemcode) {
	this.itemcode = itemcode;
}
public String getItemname() {
	return itemname;
}
public void setItemname(String itemname) {
	this.itemname = itemname;
}
public String getWeight() {
	return weight;
}
public void setWeight(String weight) {
	this.weight = weight;
}
public int getItemstock() {
	return itemstock;
}
public void setItemstock(int itemstock) {
	this.itemstock = itemstock;
}
public int getItemprice() {
	return itemprice;
}
public void setItemprice(int itemprice) {
	this.itemprice = itemprice;
}
@Override
public String toString() {
	return "ItemsDTO [sortcode=" + sortcode + ", compcode=" + compcode + ", itemcode=" + itemcode + ", itemname="
			+ itemname + ", weight=" + weight + ", itemstock=" + itemstock + ", itemprice=" + itemprice + "]";
}

}
