package editERP.stock;

public class StockDTO {
	public String sortname;
	public String compname;
	public int itemcode;
	public String itemname;
	public String weight;
	public int itemstock;

	public StockDTO() {
		// TODO Auto-generated constructor stub
	}

	public String getSortname() {
		return sortname;
	}

	public void setSortname(String sortname) {
		this.sortname = sortname;
	}

	public String getCompname() {
		return compname;
	}

	public void setCompname(String compname) {
		this.compname = compname;
	}

	public int getItemcode() {
		return itemcode;
	}

	public void setItemcode(int itemcode) {
		this.itemcode = itemcode;
	}

	public String getItemname() {
		return itemname;
	}

	public void setItemname(String itemname) {
		this.itemname = itemname;
	}

	public String getWeight() {
		return weight;
	}

	public void setWeight(String weight) {
		this.weight = weight;
	}

	public int getItemstock() {
		return itemstock;
	}

	public void setItemstock(int itemstock) {
		this.itemstock = itemstock;
	}

	public StockDTO(String sortname, String compname, int itemcode, String itemname, String weight, int itemstock) {
		super();
		this.sortname = sortname;
		this.compname = compname;
		this.itemcode = itemcode;
		this.itemname = itemname;
		this.weight = weight;
		this.itemstock = itemstock;
	}

	public StockDTO(int itemstock, int itemcode) {
		this.itemstock = itemstock;
		this.itemcode = itemcode;
	}

	@Override
	public String toString() {
		return "StockDTO [sortname=" + sortname + ", compname=" + compname + ", itemcode=" + itemcode + ", itemname="
				+ itemname + ", weight=" + weight + ", itemstock=" + itemstock + "]";
	}

}
