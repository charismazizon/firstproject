package editERP.stock;

public class CompanyDTO {
private int compcode;
private String compname;
public CompanyDTO() {
	// TODO Auto-generated constructor stub
}
public CompanyDTO(int compcode, String compname) {
	super();
	this.compcode = compcode;
	this.compname = compname;
}
public int getCompcode() {
	return compcode;
}
public void setCompcode(int compcode) {
	this.compcode = compcode;
}
public String getCompname() {
	return compname;
}
public void setCompname(String compname) {
	this.compname = compname;
}
@Override
public String toString() {
	return "CompanyDTO [compcode=" + compcode + ", compname=" + compname + "]";
}

}
