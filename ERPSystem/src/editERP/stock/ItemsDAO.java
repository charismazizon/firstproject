package editERP.stock;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Vector;

import editERP.DB;

public class ItemsDAO {
	public Vector ipgolist(String itemname) {
		Vector items = new Vector<>();
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			conn = DB.oraConn();
			StringBuilder sb = new StringBuilder();
			sb.append("SELECT s.sortname as 분류, c.compname as 제조사, i.itemcode as 제품코드,");
			sb.append(" i.itemname as 제품명, i.weight as 중량, io.amount as 수량, io.total 총액,");
			sb.append(" io.reqdate as 신청일자, io.indate as 입고일자, sel.martname as 거래처,");
			sb.append(" sel.dealer as 담당자, sel.dealertel as 연락처");
			sb.append(" FROM items i");
			sb.append(" LEFT JOIN   sortlist s  ON (i.sortcode = s.sortcode)");
			sb.append(" LEFT JOIN   company c   ON (i.compcode = c.compcode)");
			sb.append(" LEFT JOIN   IOList io   ON (io.itemcode = i.itemcode)");
			sb.append(" LEFT JOIN   seller sel  ON (io.martcode = sel.martcode)");
			pstmt = conn.prepareStatement(sb.toString());
			pstmt.setString(1, "%" + itemname + "%");
			rs = pstmt.executeQuery();
			while (rs.next()) {
				Vector row = new Vector<>();
				row.add(rs.getString("분류"));
				row.add(rs.getString("제조사"));
				row.add(rs.getInt("제품코드"));
				row.add(rs.getString("제품명"));
				row.add(rs.getString("중량"));
				row.add(rs.getInt("수량"));
				row.add(rs.getInt("총액"));
				row.add(rs.getDate("신청일자"));
				row.add(rs.getDate("입고일자"));
				row.add(rs.getString("거래처"));
				row.add(rs.getString("담당자"));
				row.add(rs.getString("연락처"));
				items.add(row);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			try {
				if (pstmt != null)
					pstmt.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			try {
				if (conn != null)
					conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return items;
	}

	public Vector searchItems(String itemname) {
		Vector items = new Vector();
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			conn = DB.oraConn();
			StringBuilder sb = new StringBuilder();
			sb.append(" SELECT s.sortname as 분류, c.compname as 제조사, i.itemcode as 제품코드, ");
			sb.append(" i.itemname as 제품명, i.weight as 중량, i.itemstock as 재고 ");
			sb.append(" FROM items i ");
			sb.append(" LEFT JOIN   sortlist s  ON  (i.sortcode = s.sortcode) ");
			sb.append(" LEFT JOIN   company c   ON  (i.compcode = c.compcode) ");
			sb.append(" where itemname like ?");
			pstmt = conn.prepareStatement(sb.toString());
			pstmt.setString(1, "%" + itemname + "%");
			rs = pstmt.executeQuery();
			while (rs.next()) {
				Vector row = new Vector<>();
				row.add(rs.getString("분류"));
				row.add(rs.getString("제조사"));
				row.add(rs.getInt("제품코드"));
				row.add(rs.getString("제품명"));
				row.add(rs.getString("중량"));
				row.add(rs.getInt("재고"));
				items.add(row);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			try {
				if (pstmt != null)
					pstmt.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			try {
				if (conn != null)
					conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return items;
	}

	public Vector listItems() {
		Vector items = new Vector<>();
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			conn = DB.oraConn();
			StringBuilder sb = new StringBuilder();
			sb.append(" SELECT s.sortname as 분류, c.compname as 제조사, i.itemcode as 제품코드, ");
			sb.append(" i.itemname as 제품명, i.weight as 중량, i.itemstock as 재고 ");
			sb.append(" FROM items i ");
			sb.append(" LEFT JOIN   sortlist s  ON  (i.sortcode = s.sortcode) ");
			sb.append(" LEFT JOIN   company c   ON  (i.compcode = c.compcode) ");
			// sb.append(" where itemcode = ?");

			pstmt = conn.prepareStatement(sb.toString());
			// pstmt.setInt(1, itemcode);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				Vector row = new Vector<>();
				row.add(rs.getString("분류"));
				row.add(rs.getString("제조사"));
				row.add(rs.getInt("제품코드"));
				row.add(rs.getString("제품명"));
				row.add(rs.getString("중량"));
				row.add(rs.getInt("재고"));
				items.add(row);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			try {
				if (pstmt != null)
					pstmt.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			try {
				if (conn != null)
					conn.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return items;
	}

	public Vector listItems(int itemcode) {
		Vector items = new Vector<>();
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			conn = DB.oraConn();
			StringBuilder sb = new StringBuilder();
			sb.append("SELECT s.sortname as 분류, c.compname as 제조사, i.itemcode as 제품코드,");
			sb.append(" i.itemname as 제품명, i.weight as 중량, i.itemstock as 재고");
			sb.append(" FROM items i");
			sb.append(" LEFT JOIN   sortlist s  ON  (i.sortcode = s.sortcode)");
			sb.append(" LEFT JOIN   company c   ON  (i.compcode = c.compcode)");
			sb.append(" where itemcode=?");
			pstmt = conn.prepareStatement(sb.toString());
			pstmt.setInt(1, itemcode);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				Vector row = new Vector<>();
				row.add(rs.getInt("itemcode"));
				row.add(rs.getString("sortname"));
				row.add(rs.getString("compname"));
				row.add(rs.getInt("weight"));
				row.add(rs.getInt("itemstock"));
				row.add(rs.getString("itemname"));
				items.add(row);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				rs.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			try {
				pstmt.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			try {
				conn.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return items;
	}
}
