package editERP.stock;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class StockEdit extends JFrame {

	private JPanel contentPane;
	private JTextField tfStock;
	private JButton btnEdit;
	
	private Stock parent;
	private StockDTO dto;

	/**
	 * Launch the application.
	 */
//	public static void main(String[] args) {
//		EventQueue.invokeLater(new Runnable() {
//			public void run() {
//				try {
//					StockEdit frame = new StockEdit();
//					frame.setVisible(true);
//				} catch (Exception e) {
//					e.printStackTrace();
//				}
//			}
//		});
//	}

	/**
	 * Create the frame.
	 */
	public StockEdit(Stock parent,StockDTO dto) {
		this.parent=parent;
		this.dto=dto;
		
		
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 415, 98);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
//		tfSortname.setText(dto.getSortname());
//		tfCompname.setText(dto.getCompname());
//		tfItemname.setText(dto.getItemname());
//		tfWeight.setText(dto.getWeight());
//		tfItemcode.setText(dto.getItemcode()+"");
//		tfStock.setText(dto.getItemstock()+"");
		
		tfStock = new JTextField();
		tfStock.setColumns(10);
		tfStock.setBounds(90, 12, 116, 24);
		contentPane.add(tfStock);
		
		JLabel label_4 = new JLabel("재고");
		label_4.setBounds(14, 15, 62, 18);
		contentPane.add(label_4);
		
		btnEdit = new JButton("수정");
		btnEdit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
//				String sortname=tfSortname.getText();
//				String compname=tfCompname.getText();
//				int itemcode=Integer.valueOf(tfItemcode.getText()+"");
//				String itemname=tfItemname.getText();
//				String weight=tfWeight.getText();
				int itemstock=Integer.valueOf(tfStock.getText());
				StockDAO dao=new StockDAO();
				int result=dao.updateStock(dto,itemstock);
				
				if(result==1){
					JOptionPane.showMessageDialog(StockEdit.this, "수정되었습니다.");
					parent.refreshModel();
					dispose();
				}
			}
		});
		btnEdit.setBounds(220, 11, 105, 27);
		contentPane.add(btnEdit);
	}
}
