package editERP.stock;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

public class StockSave extends JFrame {

	private JPanel contentPane;
	private JTextField tfSortname;
	private JTextField tfCompname;
	private JTextField tfItemcode;
	private JTextField tfItemname;
	private JTextField tfWeight;
	private JTextField tfStock;
	private JButton btnSave;
	
	private Stock frm;
	private StockDTO dto;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					StockSave frame = new StockSave();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public StockSave(Stock frm){
		this();
		this.frm=frm;
	}
	
	public StockSave() {
		
		
		
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 263, 313);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("분류");
		lblNewLabel.setBounds(14, 12, 62, 18);
		contentPane.add(lblNewLabel);
		
		tfSortname = new JTextField();
		tfSortname.setBounds(90, 9, 116, 24);
		contentPane.add(tfSortname);
		tfSortname.setColumns(10);
		
		tfCompname = new JTextField();
		tfCompname.setColumns(10);
		tfCompname.setBounds(90, 42, 116, 24);
		contentPane.add(tfCompname);
		
//		tfSortname.setText(dto.getSortname());
//		tfCompname.setText(dto.getCompname());
//		tfItemcode.setText(dto.getItemcode()+"");
//		tfItemname.setText(dto.getItemname());
//		tfWeight.setText(dto.getWeight());
//		tfStock.setText(dto.getItemstock()+"");
		
		JLabel label = new JLabel("제조사");
		label.setBounds(14, 45, 62, 18);
		contentPane.add(label);
		
		tfItemcode = new JTextField();
		tfItemcode.setColumns(10);
		tfItemcode.setBounds(90, 78, 116, 24);
		contentPane.add(tfItemcode);
		
		JLabel label_1 = new JLabel("제품코드");
		label_1.setBounds(14, 81, 62, 18);
		contentPane.add(label_1);
		
		tfItemname = new JTextField();
		tfItemname.setColumns(10);
		tfItemname.setBounds(90, 114, 116, 24);
		contentPane.add(tfItemname);
		
		JLabel label_2 = new JLabel("제품명");
		label_2.setBounds(14, 117, 62, 18);
		contentPane.add(label_2);
		
		tfWeight = new JTextField();
		tfWeight.setColumns(10);
		tfWeight.setBounds(90, 150, 116, 24);
		contentPane.add(tfWeight);
		
		JLabel label_3 = new JLabel("중량");
		label_3.setBounds(14, 153, 62, 18);
		contentPane.add(label_3);
		
		tfStock = new JTextField();
		tfStock.setColumns(10);
		tfStock.setBounds(90, 186, 116, 24);
		contentPane.add(tfStock);
		
		JLabel label_4 = new JLabel("재고");
		label_4.setBounds(14, 189, 62, 18);
		contentPane.add(label_4);
		
		
//		btnEdit = new JButton("수정");
//		btnEdit.addActionListener(new ActionListener() {
//			public void actionPerformed(ActionEvent e) {
//				String sortname=tfSortname.getText();
//				String compname=tfCompname.getText();
//				int itemcode=Integer.valueOf(tfItemcode.getText()+"");
//				String itemname=tfItemname.getText();
//				String weight=tfWeight.getText();
//				int itemstock=Integer.valueOf(tfStock.getText()+"");
//				StockDTO dto=new StockDTO(sortname, compname, itemcode, itemname, weight, itemstock);
//				StockDAO dao=new StockDAO();
//				int result=dao.updateStock(dto);
//				if(result==1){
//					JOptionPane.showMessageDialog(StockSave.this, "수정되었습니다.");
//					frm.refreshModel();
//					dispose();
		
		btnSave = new JButton("저장");
		btnSave.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String sortname=tfSortname.getText();
				String compname=tfCompname.getText();
				int itemcode=Integer.valueOf(tfItemcode.getText()+"");
				String itemname=tfItemname.getText();
				String weight=tfWeight.getText();
				int itemstock=Integer.valueOf(tfStock.getText()+"");
				StockDTO dto=new StockDTO(sortname, compname, itemcode, itemname, weight, itemstock);
				StockDAO dao=new StockDAO();
				int result=dao.updateStock(dto, itemstock);
				if(result==1){
					JOptionPane.showMessageDialog(StockSave.this, "저장되었습니다.");
					frm.refreshModel();
					dispose();
				}
			}
		});
		btnSave.setBounds(90, 222, 105, 27);
		contentPane.add(btnSave);
	}
}
