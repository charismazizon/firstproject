package editERP.stock;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import editERP.DB;

public class CompanyDAO {
public ArrayList<CompanyDTO> listcompany(){
	ArrayList<CompanyDTO> items=new ArrayList<>();
	Connection conn=null;
	PreparedStatement pstmt=null;
	ResultSet rs=null;
	try {
		conn=DB.oraConn();
		String sql="select * from company order by compcode";
		pstmt=conn.prepareStatement(sql);
		rs=pstmt.executeQuery();
		while(rs.next()){
			int compcode=rs.getInt("compcode");
			String compname=rs.getString("compname");
			CompanyDTO dto=new CompanyDTO(compcode, compname);
			items.add(dto);
		}
	} catch (Exception e) {
		e.printStackTrace();
	}finally{
		try {
			if(rs!=null)rs.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			if(pstmt!=null)pstmt.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			if(conn!=null)conn.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	return items;
}
}
