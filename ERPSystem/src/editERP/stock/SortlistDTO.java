package editERP.stock;

public class SortlistDTO {
private int sortcode;
private String sortname;
public SortlistDTO() {
	// TODO Auto-generated constructor stub
}
public SortlistDTO(int sortcode, String sortname) {
	super();
	this.sortcode = sortcode;
	this.sortname = sortname;
}
public int getSortcode() {
	return sortcode;
}
public void setSortcode(int sortcode) {
	this.sortcode = sortcode;
}
public String getSortname() {
	return sortname;
}
public void setSortname(String sortname) {
	this.sortname = sortname;
}
@Override
public String toString() {
	return "SortlistDTO [sortcode=" + sortcode + ", sortname=" + sortname + "]";
}

}
