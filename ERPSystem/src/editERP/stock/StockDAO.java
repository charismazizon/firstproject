package editERP.stock;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import editERP.DB;

public class StockDAO {
public int updateStock(StockDTO dto,int itemstock){
	int result=0;
	Connection conn=null;
	PreparedStatement pstmt=null;
	try {
		conn=DB.oraConn();
		String sql="update items set itemstock=? where itemcode=?";
		pstmt=conn.prepareStatement(sql);
		pstmt.setInt(1, itemstock);
		pstmt.setInt(2, dto.getItemcode());
		result=pstmt.executeUpdate();
	} catch (Exception e) {
		e.printStackTrace();
	}finally{
		try {
			if(pstmt!=null)pstmt.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			if(conn!=null)conn.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	return result;
}
//public int insertStock(StockDTO dto){
//	int result=0;
//	Connection conn=null;
//	PreparedStatement pstmt=null;
//	try {
//		conn=DB.oraConn();
//		StringBuilder sb=new StringBuilder();
//		sb.append("insert into items values (?,?,?,?)");
//		sb.append("insert into sortlist values (?)");
//		sb.append("insert into company values (?)");
//		pstmt=conn.prepareStatement(sb.toString());
//		pstmt.setString(1, dto.getItemname());
//		pstmt.setInt(2, dto.getItemcode());
//		pstmt.setString(3, dto.getWeight());
//		pstmt.setInt(4, dto.getItemstock());
//		pstmt.setString(5, dto.getCompname());
//		pstmt.setString(6, dto.getSortname());
//		result=pstmt.executeUpdate();
//	} catch (Exception e) {
//		e.printStackTrace();
//	}finally{
//		try {
//			if(pstmt!=null)pstmt.close();
//		} catch (SQLException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		try {
//			if(conn!=null)conn.close();
//		} catch (SQLException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//	}
//	return result;
//}
}
