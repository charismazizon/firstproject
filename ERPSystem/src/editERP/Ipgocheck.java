package editERP;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import editERP.stock.CompanyDAO;
import editERP.stock.ItemsDAO;
import editERP.stock.SortlistDAO;
import editERP.stock.StockDTO;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class Ipgocheck extends JFrame {

	private JPanel contentPane;
	private JTextField tfSearch;
	private JTable table;
	private Main parent;
	private StockDTO dto;
	
	private SortlistDAO sortlistDao;
	private CompanyDAO companyDao;
	private ItemsDAO itemsDao;
	private Vector cols;
	private DefaultTableModel model;
	private Main form;

	/**
	 * Launch the application.
	 */
//	public static void main(String[] args) {
//		EventQueue.invokeLater(new Runnable() {
//			public void run() {
//				try {
//					Stock frame = new Stock();
//					frame.setVisible(true);
//				} catch (Exception e) {
//					e.printStackTrace();
//				}
//			}
//		});
//	}

	/**
	 * Create the frame.
	 */
//	public Ipgocheck(Main parent,StockDTO dto){
//		this.parent=parent;
//		this.dto=dto;
//	}
	
	public Ipgocheck(Main form) {
		this();
		this.form=form;
	}
	
	

	public Ipgocheck() {
		cols=new Vector();
		cols.add("분류");
		cols.add("제조사");
		cols.add("제품코드");
		cols.add("제품명");
		cols.add("중량");
		cols.add("수량");
		cols.add("총액");
		cols.add("신청일자");
		cols.add("입고일자");
		cols.add("거래처");
		cols.add("담당자");
		cols.add("연락처");
		itemsDao=new ItemsDAO();
		
		setTitle("입고현황");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 1000, 850);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		JPanel panel = new JPanel();
		contentPane.add(panel, BorderLayout.NORTH);
		
		JLabel lblNewLabel = new JLabel("제품명 : ");
		panel.add(lblNewLabel);
		
		tfSearch = new JTextField();
		tfSearch.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent e) {
			search();
			}
		});
		panel.add(tfSearch);
		tfSearch.setColumns(10);
		
		JButton btnNewButton = new JButton("검색");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			search();
			}
		});
		panel.add(btnNewButton);
		
		JScrollPane scrollPane = new JScrollPane();
		contentPane.add(scrollPane, BorderLayout.CENTER);
		
		table = new JTable();
		table.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				
			}
		});
		scrollPane.setViewportView(table);
		refreshModel();
	}
	public void refreshModel() {
		model=new DefaultTableModel(itemsDao.listItems(), cols) {
			@Override
		public boolean isCellEditable(int row, int column) {
			return false;
		}
	};
		table.setModel(model);
	}
	public void search() {
		String itemname=tfSearch.getText();
		model=new DefaultTableModel(itemsDao.searchItems(itemname), cols) {
			@Override
		public boolean isCellEditable(int row, int column) {
			return false;
		}
	};
	table.setModel(model);
	}

}
