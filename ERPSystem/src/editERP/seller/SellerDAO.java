package editERP.seller;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Vector;

import editERP.DB;

public class SellerDAO {
	
	public Vector searchSeller(String martname, String dealer) {
		Vector vec = new Vector();
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		try {
			con = DB.oraConn();
			StringBuilder sql = new StringBuilder();
			sql.append(" SELECT * "); 
			sql.append(" FROM seller ");
			sql.append(" WHERE martname LIKE ? "); 
			sql.append(" AND dealer LIKE ? ");
			pst = con.prepareStatement(sql.toString());
			pst.setString(1, "%"+martname+"%");
			pst.setString(2, "%"+dealer+"%");
			rs = pst.executeQuery();
			while(rs.next()){
				Vector row = new Vector();
				row.add(rs.getInt("martcode"));
				row.add(rs.getString("martname"));
				row.add(rs.getString("marttel"));
				row.add(rs.getString("martadd"));
				row.add(rs.getString("dealer"));
				row.add(rs.getString("dealertel"));
				vec.add(row);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if(rs != null) rs.close();
			} catch (SQLException e2) {
				e2.printStackTrace();
			}
			try {
				if(pst != null) pst.close();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
			try {
				if(con != null) con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return vec;
	}
	
	public int updateSeller(SellerDTO dto) {
		int result = 0;
		Connection conn = null;
		PreparedStatement pstmt = null;
		try {
			conn = DB.oraConn();
			StringBuilder sb = new StringBuilder();
			sb.append("update seller ");
			sb.append(" set martname=?,");
			sb.append("marttel=?,martadd=?,");
			sb.append("dealer=?,dealertel=? where martcode=?");
			pstmt = conn.prepareStatement(sb.toString());
			// ? 순번과 자료형이 일치해야 함
			pstmt.setString(1, dto.getMartname());
			pstmt.setString(2, dto.getMarttel());
			pstmt.setString(3, dto.getMartadd());
			pstmt.setString(4, dto.getDealer());
			pstmt.setString(5, dto.getDealertel());
			pstmt.setInt(6, dto.getMartcode());
			result = pstmt.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (pstmt != null)
					pstmt.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			try {
				if (conn != null)
					conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return result;
	}

	public int insertSeller(SellerDTO dto) {
		int result = 0;
		Connection conn = null;
		PreparedStatement pstmt = null;
		try { // java code에서는 auto commit
			conn = DB.oraConn(); // 오라클 서버에 접속
			StringBuilder sb = new StringBuilder();
			sb.append("insert into seller ");
			sb.append("values (?,?,?,?,?,?)");
			pstmt = conn.prepareStatement(sb.toString());
			pstmt.setInt(1, dto.getMartcode());
			pstmt.setString(2, dto.getMartname());
			pstmt.setString(3, dto.getMarttel());
			pstmt.setString(4, dto.getMartadd());
			pstmt.setString(5, dto.getDealer());
			pstmt.setString(6, dto.getDealertel());
			// 성공하면 1, 실패하면 0가 리턴
			// affected rows(영향을 받은 행의 수)
			result = pstmt.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (pstmt != null)
					pstmt.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			try {
				if (conn != null)
					conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return result;
	}

	public SellerDTO viewSeller(int martcode) {
		SellerDTO dto = null;
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			conn = DB.oraConn();
			StringBuilder sb = new StringBuilder();
			sb.append("select martname,");
			sb.append("martname,marttel");
			sb.append(",martadd, dealer");
			sb.append(",dealertel from seller");
			sb.append(" where martcode=? ");
			pstmt = conn.prepareStatement(sb.toString());
			pstmt.setInt(1, martcode);
			rs = pstmt.executeQuery();
			if (rs.next()) {
				String martname = rs.getString("martname");
				String marttel = rs.getString("marttel");
				String martadd = rs.getString("martadd");
				String dealer = rs.getString("dealer");
				String dealertel = rs.getString("dealertel");
				dto = new SellerDTO(martcode, martname, marttel, martadd, dealer, dealertel);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			try {
				if (pstmt != null)
					pstmt.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			try {
				if (conn != null)
					conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return dto;
	}

	public Vector listSeller() {
		Vector items = new Vector();
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			conn = DB.oraConn();
			String sql = "select * from seller order by martcode";
			pstmt = conn.prepareStatement(sql);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				Vector row = new Vector<>();
				int martcode = rs.getInt("martcode");
				String martname = rs.getString("martname");
				String marttel = rs.getString("marttel");
				String martadd = rs.getString("martadd");
				String dealer = rs.getString("dealer");
				String dealertel = rs.getString("dealertel");

				row.add(rs.getInt("martcode"));
				row.add(rs.getString("martname"));
				row.add(rs.getString("marttel"));
				row.add(rs.getString("martadd"));
				row.add(rs.getString("dealer"));
				row.add(rs.getString("dealertel"));
				items.add(row);
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			try {
				if (pstmt != null)
					pstmt.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			try {
				if (conn != null)
					conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return items;
	}
	
	public int deleteSeller(int martcode){
		int result = 0;
		Connection conn =null;
		PreparedStatement pstmt=null;
		try {
			conn =DB.oraConn();
			StringBuilder sb=new StringBuilder();
			sb.append("delete from Seller where martcode=? ");
			pstmt = conn.prepareStatement(sb.toString());
			pstmt.setInt(1, martcode);
			result =pstmt.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if(pstmt != null) pstmt.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			try {
				if(conn != null) conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
	}
		return result;
	}
}
