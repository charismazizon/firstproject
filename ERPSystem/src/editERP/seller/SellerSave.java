package editERP.seller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

public class SellerSave extends JFrame {

	private JPanel contentPane;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;
	private JTextField textField_3;
	private JTextField textField_4;
	private JTextField textField_5;
	// 추가
	private SellerList frm;

	/**
	 * Launch the application.
	 */

	public SellerSave(SellerList frm) {
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel label = new JLabel("거래처 코드");
		label.setBounds(12, 13, 95, 15);
		contentPane.add(label);

		textField = new JTextField();
		textField.setColumns(10);
		textField.setBounds(114, 7, 116, 21);
		contentPane.add(textField);

		JLabel label_1 = new JLabel("거래처 이름");
		label_1.setBounds(12, 42, 95, 15);
		contentPane.add(label_1);

		textField_1 = new JTextField();
		textField_1.setColumns(10);
		textField_1.setBounds(114, 36, 116, 21);
		contentPane.add(textField_1);

		JLabel label_2 = new JLabel("거래처 전화번호");
		label_2.setBounds(12, 73, 95, 15);
		contentPane.add(label_2);

		textField_2 = new JTextField();
		textField_2.setColumns(10);
		textField_2.setBounds(114, 67, 116, 21);
		contentPane.add(textField_2);

		JLabel label_3 = new JLabel("주소");
		label_3.setBounds(12, 108, 95, 15);
		contentPane.add(label_3);

		textField_3 = new JTextField();
		textField_3.setColumns(10);
		textField_3.setBounds(114, 102, 116, 21);
		contentPane.add(textField_3);

		JLabel label_4 = new JLabel("거래인");
		label_4.setBounds(12, 139, 95, 15);
		contentPane.add(label_4);

		textField_4 = new JTextField();
		textField_4.setColumns(10);
		textField_4.setBounds(114, 133, 116, 21);
		contentPane.add(textField_4);

		JButton button = new JButton("저장");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int martcode = Integer.parseInt(textField.getText());
				String martname = textField_1.getText();
				String marttel = textField_2.getText();
				String martadd = textField_3.getText();
				String dealler = textField_4.getText();
				String deallertel = textField_5.getText();
				SellerDTO dto = new SellerDTO(martcode, martname, marttel, martadd, dealler, deallertel);
				SellerDAO dao = new SellerDAO();
				int result = dao.insertSeller(dto);
				if (result == 1) {
					JOptionPane.showMessageDialog(SellerSave.this, "저장완료.");
					frm.refreshTable();
					dispose();
				}
			}
		});
		button.setBounds(72, 206, 97, 23);
		contentPane.add(button);

		textField_5 = new JTextField();
		textField_5.setColumns(10);
		textField_5.setBounds(114, 164, 116, 21);
		contentPane.add(textField_5);

		JLabel label_5 = new JLabel("거래인 전화번호");
		label_5.setBounds(12, 166, 95, 15);
		contentPane.add(label_5);
	}

}
