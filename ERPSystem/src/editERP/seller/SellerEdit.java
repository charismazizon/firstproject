package editERP.seller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

public class SellerEdit extends JFrame {
	// 추가
	private SellerList parent;
	private SellerDAO dao;
	private SellerDTO dto;
	private int martcode;

	private JPanel contentPane;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;
	private JTextField textField_3;
	private JTextField textField_4;
	private JTextField textField_5;

	/**
	 * Create the frame.
	 */
	public SellerEdit(SellerList parent, int martcode) {

		// this.parent = parent;
		// this.martcode=martcode;
		setTitle("편집창");
		// 현재 창만 닫는 옵션으로 변경
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 265, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel label = new JLabel("거래처 코드");
		label.setBounds(12, 16, 95, 15);
		contentPane.add(label);

		textField = new JTextField();
		textField.setEditable(false);
		textField.setColumns(10);
		textField.setBounds(114, 10, 116, 21);
		contentPane.add(textField);

		JLabel label_1 = new JLabel("거래처 이름");
		label_1.setBounds(12, 45, 95, 15);
		contentPane.add(label_1);

		textField_1 = new JTextField();
		textField_1.setColumns(10);
		textField_1.setBounds(114, 39, 116, 21);
		contentPane.add(textField_1);

		JLabel label_2 = new JLabel("거래처 전화번호");
		label_2.setBounds(12, 76, 95, 15);
		contentPane.add(label_2);

		textField_2 = new JTextField();
		textField_2.setColumns(10);
		textField_2.setBounds(114, 70, 116, 21);
		contentPane.add(textField_2);

		JLabel label_3 = new JLabel("주소");
		label_3.setBounds(12, 111, 95, 15);
		contentPane.add(label_3);

		textField_3 = new JTextField();
		textField_3.setColumns(10);
		textField_3.setBounds(114, 105, 116, 21);
		contentPane.add(textField_3);

		JLabel label_4 = new JLabel("거래인");
		label_4.setBounds(12, 142, 95, 15);
		contentPane.add(label_4);

		textField_4 = new JTextField();
		textField_4.setColumns(10);
		textField_4.setBounds(114, 136, 116, 21);
		contentPane.add(textField_4);

		JLabel label_5 = new JLabel("거래인 전화번호");
		label_5.setBounds(12, 169, 95, 15);
		contentPane.add(label_5);

		textField_5 = new JTextField();
		textField_5.setColumns(10);
		textField_5.setBounds(114, 167, 116, 21);
		contentPane.add(textField_5);

		dao = new SellerDAO();
		dto = dao.viewSeller(martcode);
		textField.setText(dto.getMartcode() + "");
		textField_1.setText(dto.getMartname());
		textField_2.setText(dto.getMarttel());
		textField_3.setText(dto.getMartadd());
		textField_4.setText(dto.getDealer());
		textField_5.setText(dto.getDealertel());

		JButton button = new JButton("저장");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int res = JOptionPane.showConfirmDialog(SellerEdit.this, "수정하시겠습니까?", "", 2);
				if (res == JOptionPane.YES_OPTION){
					int martcode = Integer.parseInt(textField.getText());
					String martname = textField_1.getText();
					String marttel = textField_2.getText();
					String martadd = textField_3.getText();
					String dealer = textField_4.getText();
					String dealertel = textField_5.getText();
					SellerDTO dto = new SellerDTO(martcode, martname, marttel, martadd, dealer, dealertel);
					int result = dao.updateSeller(dto);
					if (result == 1) { // update 성공
						JOptionPane.showMessageDialog(SellerEdit.this, "수정되었습니다.");
						parent.refreshTable();
						dispose();	
					}else{
						JOptionPane.showMessageDialog(SellerEdit.this, "실패함.");
					}
				}
			}
		});
		button.setBounds(65, 208, 97, 23);
		contentPane.add(button);
	}

}
