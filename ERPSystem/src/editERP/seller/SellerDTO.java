package editERP.seller;

public class SellerDTO {
private int martcode;
private String martname;
private String marttel;
private String martadd;
private String dealer;
private String dealertel;
public SellerDTO() {
	// TODO Auto-generated constructor stub
}
public SellerDTO(int martcode, String martname, String marttel, String martadd, String dealer, String dealertel) {
	this.martcode = martcode;
	this.martname = martname;
	this.marttel = marttel;
	this.martadd = martadd;
	this.dealer = dealer;
	this.dealertel = dealertel;
}
public int getMartcode() {
	return martcode;
}
public void setMartcode(int martcode) {
	this.martcode = martcode;
}
public String getMartname() {
	return martname;
}
public void setMartname(String martname) {
	this.martname = martname;
}
public String getMarttel() {
	return marttel;
}
public void setMarttel(String marttel) {
	this.marttel = marttel;
}
public String getMartadd() {
	return martadd;
}
public void setMartadd(String martadd) {
	this.martadd = martadd;
}
public String getDealer() {
	return dealer;
}
public void setDealer(String dealer) {
	this.dealer = dealer;
}
public String getDealertel() {
	return dealertel;
}
public void setDealertel(String dealertel) {
	this.dealertel = dealertel;
}
@Override
public String toString() {
	return "SellerDTO [martcode=" + martcode + ", martname=" + martname + ", marttel=" + marttel + ", martadd="
			+ martadd + ", dealer=" + dealer + ", dealertel=" + dealertel + "]";
}

}
