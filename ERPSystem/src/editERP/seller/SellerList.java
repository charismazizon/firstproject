package editERP.seller;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;
import javax.swing.JTextField;
import javax.swing.JLabel;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

public class SellerList extends JFrame {

	// 변수 추가
	private SellerDAO dao;
	private Vector cols; // 테이블의 제목 컬럼
	private int idx = -1;

	private JPanel contentPane;
	private JTable table;
	private JTextField textField;
	private JTextField textField_1;
	private String lv;
	/**
	 * Launch the application.
	 */
//	public static void main(String[] args) {
//		EventQueue.invokeLater(new Runnable() {
//			public void run() {
//				try {
//					SellerList frame = new SellerList();
//					frame.setVisible(true);
//				} catch (Exception e) {
//					e.printStackTrace();
//				}
//			}
//		});
//	}

	/**
	 * Create the frame.
	 */
	public SellerList(String lv3) {
		this.lv = lv3;
		setTitle("거래처 관리");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 723, 463);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new BorderLayout(0, 0));

		JPanel panel = new JPanel();
		contentPane.add(panel, BorderLayout.NORTH);
		JScrollPane scrollPane = new JScrollPane();
		contentPane.add(scrollPane, BorderLayout.CENTER);

		table = new JTable();
		table.setFont(new Font("맑은 고딕", Font.PLAIN, 12));
		table.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				idx = table.getSelectedRow();
			}
		});
		scrollPane.setViewportView(table);
		dao = new SellerDAO();
		cols = new Vector();
		cols.add("거래처 코드");
		cols.add("거래처 이름");
		cols.add("거래처 전화번호");
		cols.add("거래처 주소");
		cols.add("거래인");
		cols.add("거래인 전화번호");
		refreshTable();
		JButton btnAdd = new JButton("추가");
		if (lv.equals("관리자")){
			btnAdd.setEnabled(true);
		}else if(lv.equals("일반")){
			btnAdd.setEnabled(false);
		}
		btnAdd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (lv.equals("관리자")){
					SellerSave frm = new SellerSave(SellerList.this);
					frm.setVisible(true);
					frm.setLocation(SellerList.this.getX(), SellerList.this.getY() + 400);					
				}else{
					JOptionPane.showMessageDialog(SellerList.this, "권한이 없습니다.");
				}
			}
		});
		
		JLabel label = new JLabel("담당자");
		panel.add(label);
		
		textField_1 = new JTextField();
		textField_1.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent e) {
				String dealer = textField_1.getText();
				String martname = textField.getText();
				DefaultTableModel model1 = new DefaultTableModel(dao.searchSeller(martname, dealer), cols){
					@Override
					public boolean isCellEditable(int row, int column) {
						return false;
					}
				};
				table.setModel(model1);
			}
		});
		panel.add(textField_1);
		textField_1.setColumns(10);
		
		JLabel lblNewLabel = new JLabel("거래처");
		panel.add(lblNewLabel);
		
		textField = new JTextField();
		textField.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent e) {
				String dealer = textField_1.getText();
				String martname = textField.getText();
				DefaultTableModel model1 = new DefaultTableModel(dao.searchSeller(martname, dealer), cols){
					@Override
					public boolean isCellEditable(int row, int column) {
						return false;
					}
				};
				table.setModel(model1);
			}
		});
		panel.add(textField);
		textField.setColumns(10);
		panel.add(btnAdd);

		JButton btnEdit = new JButton("편집");
		if (lv.equals("관리자")){
			btnEdit.setEnabled(true);
		}else if(lv.equals("일반")){
			btnEdit.setEnabled(false);
		}
		btnEdit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(lv.equals("관리자")){
					// JTable에서 선택한 행의 인덱스값
					int idx = table.getSelectedRow();
					if (idx == -1) { // 선택한 행이 없으면
						JOptionPane.showMessageDialog(SellerList.this, "편집할 자료를 선택하세요.");
						return;
					} else {
						int martcode = Integer.parseInt(table.getValueAt(idx, 0) + "");
						SellerEdit frm = new SellerEdit(SellerList.this, martcode);
						frm.setVisible(true);
						frm.setLocation(SellerList.this.getX(), SellerList.this.getY() + 400);
						idx = -1;
					}
				}else{
					JOptionPane.showMessageDialog(SellerList.this, "권한이 없습니다.");
				}
			}
		});
		panel.add(btnEdit);

		JButton btnDelete = new JButton("삭제");
		if (lv.equals("관리자")){
			btnDelete.setEnabled(true);
		}else if(lv.equals("일반")){
			btnDelete.setEnabled(false);
		}
		btnDelete.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (lv.equals("관리자")){
					if (idx == -1) {
						JOptionPane.showMessageDialog(SellerList.this, "삭제할 데이터를 선택하세요.");
					} else {
						int martcode = Integer.valueOf(table.getValueAt(idx, 0) + "");
						int response = JOptionPane.showConfirmDialog(SellerList.this, "삭제하시겠습니까?", "", 2);
						if (response == JOptionPane.YES_OPTION) {
							int result = dao.deleteSeller(martcode);
							if (result == 1) {
								JOptionPane.showMessageDialog(SellerList.this, "삭제되었습니다");
								refreshTable();
							}
						}
						idx = -1;
					}
				}else{
					JOptionPane.showMessageDialog(SellerList.this, "권한이 없습니다.");
				}
			}
		});
		panel.add(btnDelete);
	}

	public void refreshTable() {
		DefaultTableModel model = new DefaultTableModel(dao.listSeller(), cols);
		table.setModel(model);
	}
}
