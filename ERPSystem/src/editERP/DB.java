package editERP;

import java.io.FileInputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.Properties;

public class DB {
	public static Connection dbConn() {
		Connection conn = null;
		try {
			FileInputStream fis = new FileInputStream("d:\\work\\db.prop");
			Properties prop = new Properties(); // key와 value를 세트로 저장
			prop.load(fis);
			String url = prop.getProperty("url");
			String id = prop.getProperty("id");
			String password = prop.getProperty("password");
			conn = DriverManager.getConnection(url, id, password); // MySQL 접속
		} catch (Exception e) {
			e.printStackTrace();
		}
		return conn;
	}

	public static Connection oraConn() {
		Connection con = null;
		try {
			FileInputStream fis = new FileInputStream("d:\\work\\oracle.prop");
			Properties prop = new Properties();
			prop.load(fis);
			String url = prop.getProperty("url");
			String id = prop.getProperty("id");
			String password = prop.getProperty("password");
			con = DriverManager.getConnection(url, id, password);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return con;
	}

	public static Connection hrConn() {
		Connection con = null;
		try {
			FileInputStream fis = new FileInputStream("d:\\work\\hr.prop");
			Properties prop = new Properties();
			prop.load(fis);
			String url = prop.getProperty("url");
			String id = prop.getProperty("id");
			String password = prop.getProperty("password");
			con = DriverManager.getConnection(url, id, password);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return con;
	}
}
