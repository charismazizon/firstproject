package editERP;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import editERP.member.MemberManage;
import editERP.notice.NoticeDTO;
import editERP.notice.NoticeText;
import editERP.seller.SellerList;
import editERP.stock.Stock;

public class Main extends JFrame {

	private JPanel contentPane;
	private JTextField tfchat;
	private JButton btnchat;
	private MemberManage editMember;
	private String lv;
	private String name;
	private NoticeText nt;
	private ArrayList<NoticeDTO> test;
	private JTextArea ta;
	/**
	 * Launch the application.
	 */
	// public static void main(String[] args) {
	// EventQueue.invokeLater(new Runnable() {
	// public void run() {
	// try {
	// Main frame = new Main();
	// frame.setVisible(true);
	// } catch (Exception e) {
	// e.printStackTrace();
	// }
	// }
	// });
	// }

	/**
	 * Create the frame.
	 */
	public Main(String lv, String name2) {
		this.lv = lv;
		this.name = name2;

		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 470, 426);

		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);

		JMenu mnNewMenu = new JMenu("시스템");
		menuBar.add(mnNewMenu);

		JMenuItem mntmNewMenuItem = new JMenuItem("로그아웃");
		mntmNewMenuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int result = JOptionPane.showConfirmDialog(Main.this, "로그아웃하시겠습니까?", "로그아웃", JOptionPane.YES_NO_OPTION);
				if (result == JOptionPane.YES_OPTION) {
					JOptionPane.showMessageDialog(Main.this, "로그아웃됐습니다.");
					dispose();
					Login frm = new Login(); // A대신 로그인 창 띄우기
					frm.setVisible(true);
					frm.setLocation(200, 200);
				}
			}
		});
		mnNewMenu.add(mntmNewMenuItem);

		JMenuItem mntmNewMenuItem_1 = new JMenuItem("프로그램 종료");
		mntmNewMenuItem_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int result = JOptionPane.showConfirmDialog(Main.this, "종료하겠습니까?", "프로그램 종료", JOptionPane.YES_NO_OPTION);
				if (result == JOptionPane.YES_OPTION) {
					JOptionPane.showMessageDialog(Main.this, "프로그램을 종료합니다.");
					System.exit(0);
				}
			}
		});
		mnNewMenu.add(mntmNewMenuItem_1);

		JMenu mnNewMenu_1 = new JMenu("재고");
		menuBar.add(mnNewMenu_1);

		JMenuItem mntmNewMenuItem_2 = new JMenuItem("재고현황");
		mntmNewMenuItem_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Stock sto = new Stock(lv);
				sto.setVisible(true);
				sto.setLocation(100, 300);
			}
		});
		mnNewMenu_1.add(mntmNewMenuItem_2);

		JMenu mnNewMenu_2 = new JMenu("발주");
		menuBar.add(mnNewMenu_2);

		JMenuItem mntmNewMenuItem_3 = new JMenuItem("발주현황");
		mnNewMenu_2.add(mntmNewMenuItem_3);

		JMenuItem menuItem = new JMenuItem("발주신청");
		mnNewMenu_2.add(menuItem);

		JMenu mnNewMenu_3 = new JMenu("입고");
		menuBar.add(mnNewMenu_3);

		JMenuItem mntmNewMenuItem_4 = new JMenuItem("입고현황");
		mntmNewMenuItem_4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Ipgocheck form = new Ipgocheck(Main.this);
				form.setVisible(true);
				form.setLocation(200, 200);
			}
		});
		mnNewMenu_3.add(mntmNewMenuItem_4);

		JMenu mnNewMenu_4 = new JMenu("직원");
		menuBar.add(mnNewMenu_4);

		JMenuItem mntmNewMenuItem_6 = new JMenuItem("직원관리");
		if (lv.equals("관리자")){
			mntmNewMenuItem_6.setEnabled(true);
		}else if(lv.equals("일반")){
			mntmNewMenuItem_6.setEnabled(false);
		}
		mntmNewMenuItem_6.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (lv.equals("관리자")) {
					editMember = new MemberManage();
					editMember.setVisible(true);
				} else {
					JOptionPane.showMessageDialog(Main.this, "권한이 없습니다.");
				}
			}
		});
		mnNewMenu_4.add(mntmNewMenuItem_6);

		JMenu mnNewMenu_5 = new JMenu("거래처");
		menuBar.add(mnNewMenu_5);

		JMenuItem mntmNewMenuItem_9 = new JMenuItem("거래처 관리");
		mntmNewMenuItem_9.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				SellerList slist = new SellerList(lv);
				slist.setVisible(true);
			}
		});
		mnNewMenu_5.add(mntmNewMenuItem_9);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		tabbedPane.setToolTipText("");
		tabbedPane.setBounds(5, 0, 437, 362);
		contentPane.add(tabbedPane);

		JPanel panel = new JPanel();
		tabbedPane.addTab("공지사항", null, panel, null);
		panel.setLayout(null);

		JScrollPane scrollPane_1 = new JScrollPane();
		scrollPane_1.setBounds(12, 34, 408, 257);
		panel.add(scrollPane_1);

		ta = new JTextArea();
		refresh();
		ta.setEditable(false);
		scrollPane_1.setViewportView(ta);

		btnchat = new JButton("공지등록");

		btnchat.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String noticetext = tfchat.getText();
				if (noticetext.equals("")) {
					JOptionPane.showMessageDialog(Main.this, "메세지 입력.");
					tfchat.requestFocus();
				} else {
					int res = nt.insertNotice(lv, name, noticetext);
					if (res == 1) {
						JOptionPane.showMessageDialog(Main.this, "공지사항 등록.");
						tfchat.setText("");
						refresh();
					} else {
						JOptionPane.showMessageDialog(Main.this, "등록 실패.");
					}
				}
			}
		});
		btnchat.setBounds(191, 301, 97, 23);
		panel.add(btnchat);

		tfchat = new JTextField();
		tfchat.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String noticetext = tfchat.getText();
				if (noticetext.equals("")) {
					JOptionPane.showMessageDialog(Main.this, "메세지 입력.");
					tfchat.requestFocus();
				} else {
					int res = nt.insertNotice(lv, name, noticetext);
					if (res == 1) {
						JOptionPane.showMessageDialog(Main.this, "공지사항 등록.");
						tfchat.setText("");
						refresh();
					} else {
						JOptionPane.showMessageDialog(Main.this, "등록 실패.");
					}
				}
			}
		});
		tfchat.setBounds(12, 302, 167, 21);
		panel.add(tfchat);
		tfchat.setColumns(10);

		JButton btnNewButton = new JButton("삭제");
		if (lv.equals("관리자")){
			btnNewButton.setEnabled(true);
		}else if(lv.equals("일반")){
			btnNewButton.setEnabled(false);
		}
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (lv.equals("관리자")) {
					int sel = JOptionPane.showConfirmDialog(Main.this, "삭제?", "", 2);
					if (sel == JOptionPane.YES_OPTION){
						if ((tfchat.getText()).equals("")){
							JOptionPane.showMessageDialog(Main.this, "삭제할 글번호를 입력.");
							tfchat.requestFocus();
						}else{
							int num = Integer.parseInt(tfchat.getText());
							int res = nt.deleteNotice(num);
							if (res == 1){
								JOptionPane.showMessageDialog(Main.this, "삭제완료.");
								tfchat.setText("");
								refresh();
							}else{
								JOptionPane.showMessageDialog(Main.this, "실패.");
							}
						}
					}
				} else {
					JOptionPane.showMessageDialog(Main.this, "권한이 없습니다.");
					tfchat.setText("");
				}
			}
		});
		btnNewButton.setBounds(306, 301, 97, 23);
		panel.add(btnNewButton);
		
		JLabel label = new JLabel("공지사항 ( 삭제는 관리자만 가능합니다.)");
		label.setBounds(12, 10, 408, 15);
		panel.add(label);

		JPanel panel_3 = new JPanel();
		tabbedPane.addTab("사용방법", null, panel_3, null);
		panel_3.setLayout(null);

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(0, 0, 432, 328);
		panel_3.add(scrollPane);

		JPanel panel_2 = new JPanel();
		tabbedPane.addTab("업데이트", null, panel_2, null);
		panel_2.setLayout(null);

		JTextArea textArea = new JTextArea();
		textArea.setText(
				"달콤한 매상을 위한 새로운  3.0  버전 업데이트\r\n* 업데이트 버전 첨부 www.getgle.com\r\n 겨울 끝판왕, 추위 잘 싸우고 계신가요?\r\n날씨와는 다르게 마음 따뜻하게 해줄  새로운 차들이 입고되었습니다.\r\n고급 원두, 차 10% , 시럽류 20% 할인 진행중 \r\n고객님들의 맘을 사로잡을 새로운 선택을 기다립니다.\r\n\r\n \r\n ");
		textArea.setBounds(0, 0, 444, 328);
		panel_2.add(textArea);
	}

	public void refresh() {
		nt = new NoticeText();
		test = nt.listNotice();
		ta.setText("");
		for (int i = 0; i < test.size(); i++) {
			ta.append(test.get(i).getNum() + "번글.  ");
			ta.append(test.get(i).getToday().toString() + " ");
			ta.append("[" + test.get(i).getMemlv() + "]");
			ta.append(test.get(i).getMemname() + "\n");
			ta.append(test.get(i).getNoticetext() + "\n\n");
		}
	}
}
