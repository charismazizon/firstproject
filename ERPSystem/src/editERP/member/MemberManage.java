package editERP.member;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.swing.ButtonGroup;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.JScrollPane;
import javax.swing.JList;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;

import editERP.DB;

import javax.swing.event.ListSelectionEvent;

public class MemberManage extends JFrame {

	private MemberDTO dto;
	private MemberDAO dao;
	private JPanel contentPane;
	private JTextField tfId;
	private JTextField tfName;
	private JTextField tfEmail;
	private JTextField tfDays;
	private JTextField tfPhone;
	private JTextField tfBirth;
	private JTextField tfAddress;
	private JTextField tfEmpno;
	private JComboBox comboLevel;
	private JRadioButton rdbtnMan;
	private JRadioButton rdbtnWoman;
	private final ButtonGroup buttonGroup = new ButtonGroup();
	private JList list;

	/**
	 * Launch the application.
	 */
//	public static void main(String[] args) {
//		EventQueue.invokeLater(new Runnable() {
//			public void run() {
//				try {
//					MemberManage frame = new MemberManage();
//					frame.setVisible(true);
//				} catch (Exception e) {
//					e.printStackTrace();
//				}
//			}
//		});
//	}

	/**
	 * Create the frame.
	 */
	public MemberManage() {
		
		dao = new MemberDAO();
		setTitle("직원관리");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 532, 450);
		contentPane = new JPanel();
		contentPane.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
			}
		});
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel lblNewLabel = new JLabel("직원 ID : ");
		lblNewLabel.setBounds(152, 28, 62, 18);
		contentPane.add(lblNewLabel);

		tfId = new JTextField();
		tfId.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				search();
			}
		});
		tfId.setBounds(216, 25, 116, 24);
		contentPane.add(tfId);
		tfId.setColumns(10);

		JButton btnNewButton = new JButton("ID 조회");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (tfId.getText().equals("")){
					JOptionPane.showMessageDialog(MemberManage.this, "아이디를 입력해주세요.");
				}else{
					search();					
				}
			}
		});

		btnNewButton.setBounds(379, 24, 105, 27);
		contentPane.add(btnNewButton);

		JLabel label = new JLabel("이   름 : ");
		label.setBounds(152, 58, 62, 18);
		contentPane.add(label);

		tfName = new JTextField();
		tfName.setColumns(10);
		tfName.setBounds(216, 56, 116, 24);
		contentPane.add(tfName);

		JLabel label_1 = new JLabel("성   별 : ");
		label_1.setBounds(152, 148, 62, 18);
		contentPane.add(label_1);

		JLabel label_2 = new JLabel("연락처 : ");
		label_2.setBounds(152, 205, 62, 18);
		contentPane.add(label_2);

		tfPhone = new JTextField();
		tfPhone.setBounds(216, 203, 175, 24);
		contentPane.add(tfPhone);
		tfPhone.setColumns(10);

		JLabel label_3 = new JLabel("이메일 : ");
		label_3.setBounds(152, 235, 62, 18);
		contentPane.add(label_3);

		tfEmail = new JTextField();
		tfEmail.setColumns(10);
		tfEmail.setBounds(216, 233, 175, 24);
		contentPane.add(tfEmail);

		JLabel label_4 = new JLabel("입사일 : ");
		label_4.setBounds(152, 265, 62, 18);
		contentPane.add(label_4);

		tfDays = new JTextField();
		tfDays.setColumns(10);
		tfDays.setBounds(216, 263, 175, 24);
		contentPane.add(tfDays);

		JLabel label_6 = new JLabel("직   급 : ");
		label_6.setBounds(152, 119, 62, 18);
		contentPane.add(label_6);

		JButton btnSave = new JButton("삭제");
		btnSave.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				String id = tfId.getText();
				if (id.equals("")) {
					JOptionPane.showMessageDialog(MemberManage.this, "조회된 아이디가 없습니다.");
				} else {
					int result = 0;
					int response = JOptionPane.showConfirmDialog(MemberManage.this, "Delete?", "", 2);
					if (response == JOptionPane.YES_OPTION) {
						result = dao.deleteMember(id);
						System.out.println(result);
						if (result == 1) {
							JOptionPane.showMessageDialog(MemberManage.this, "삭제되었습니다.");
							tfId.setText("");
							tfName.setText("");
							tfEmail.setText("");
							tfDays.setText("");
							tfPhone.setText("");
							tfBirth.setText("");
							tfAddress.setText("");
							tfEmpno.setText("");
						}
					}
				}
			}
		});
		btnSave.setBounds(290, 347, 80, 27);
		contentPane.add(btnSave);

		JButton btnUpdate = new JButton("수정");
		btnUpdate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				
				if (tfName.getText().equals("")) {
					JOptionPane.showMessageDialog(MemberManage.this, "아이디 검색을 먼저 해주시기 바랍니다.");
					tfId.requestFocus();
				} else {
					int res = JOptionPane.showConfirmDialog(MemberManage.this, "수정?", "",
							JOptionPane.YES_NO_OPTION);
					if (res == JOptionPane.YES_OPTION){
						input();
						int result = dao.updateMember(dto);
						if (result == 1) {
							clear();
							JOptionPane.showMessageDialog(MemberManage.this, "수정되었습니다");
						}
					}
				}

			}
		});
		btnUpdate.setBounds(179, 347, 80, 27);
		contentPane.add(btnUpdate);

		JButton btnCancel = new JButton("초기화");
		btnCancel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int result = JOptionPane.showConfirmDialog(MemberManage.this, "초기화?", "",
						JOptionPane.YES_NO_OPTION);
				if (result == JOptionPane.YES_OPTION) {
					clear();
					JOptionPane.showMessageDialog(MemberManage.this, "초기화 완료.");
				}
			}
		});
		btnCancel.setBounds(404, 347, 80, 27);
		contentPane.add(btnCancel);

		JLabel label_7 = new JLabel("생년월일 :");
		label_7.setBounds(152, 175, 80, 18);
		contentPane.add(label_7);

		tfBirth = new JTextField();
		tfBirth.setBounds(216, 173, 116, 24);
		contentPane.add(tfBirth);
		tfBirth.setColumns(10);

		JLabel lblNewLabel_1 = new JLabel("주   소 :");
		lblNewLabel_1.setBounds(152, 296, 62, 18);
		contentPane.add(lblNewLabel_1);

		tfAddress = new JTextField();
		tfAddress.setBounds(217, 293, 174, 24);
		contentPane.add(tfAddress);
		tfAddress.setColumns(10);

		JLabel label_8 = new JLabel("사   번 :");
		label_8.setBounds(152, 88, 62, 18);
		contentPane.add(label_8);

		tfEmpno = new JTextField();
		tfEmpno.setEditable(false);
		tfEmpno.setBounds(216, 86, 116, 24);
		contentPane.add(tfEmpno);
		tfEmpno.setColumns(10);

		rdbtnWoman = new JRadioButton("여성");
		buttonGroup.add(rdbtnWoman);
		rdbtnWoman.setBounds(275, 143, 57, 27);
		contentPane.add(rdbtnWoman);

		rdbtnMan = new JRadioButton("남성");
		buttonGroup.add(rdbtnMan);
		rdbtnMan.setBounds(216, 143, 62, 27);
		contentPane.add(rdbtnMan);

		comboLevel = new JComboBox();
		comboLevel.setModel(new DefaultComboBoxModel(new String[] { "", "일반", "관리자" }));
		comboLevel.setBounds(216, 116, 116, 21);
		contentPane.add(comboLevel);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(12, 10, 116, 392);
		contentPane.add(scrollPane);

		ArrayList<String> memberList=dao.listMember();

		list = new JList(memberList.toArray());
		list.addListSelectionListener(new ListSelectionListener() {
			public void valueChanged(ListSelectionEvent e) {
				String str=list.getSelectedValue().toString();
				//System.out.println(str);
				String[] arr=str.split(" ");
				int empno=Integer.valueOf(arr[0]);
				//System.out.println("사번 : "+empno);
				dto=dao.detailMember2(empno);
				if (dto != null) {
					tfId.setText(dto.getId());
					tfName.setText(dto.getName());
					tfEmail.setText(dto.getEmail());
					tfDays.setText(String.valueOf(dto.getDays()));
					tfPhone.setText(dto.getPhone());
					if (dto.getGender().equals("남성")) {
						rdbtnMan.setSelected(true);
						rdbtnWoman.setSelected(false);
					} else if (dto.getGender().equals("여성")){
						rdbtnMan.setSelected(false);
						rdbtnWoman.setSelected(true);
					} else {
						buttonGroup.clearSelection();
						JOptionPane.showMessageDialog(MemberManage.this, "성별을 선택하세요.");
					}
					comboLevel.setSelectedItem(dto.getLv());
					tfAddress.setText(dto.getAddress1());
					tfBirth.setText(String.valueOf(dto.getBirthday()));
					tfEmpno.setText(String.valueOf(dto.getEmpno()));
				}
				
			}
		});
		scrollPane.setViewportView(list);
	}

	public void input() {
		String id = tfId.getText();
		String name = tfName.getText();
		int empno = Integer.valueOf(tfEmpno.getText());
		String phone = tfPhone.getText();
		String email = tfEmail.getText();
		Date day = Date.valueOf(tfDays.getText());
		Date birthday = Date.valueOf(tfBirth.getText());
		String adress = tfAddress.getText();
		String gender = "";
		if (rdbtnMan.isSelected() == true) {
			gender = "남성";
		} else if (rdbtnWoman.isSelected() == true) {
			gender = "여성";
		}
		String level = comboLevel.getSelectedItem() + "";
		dto = new MemberDTO(id, name, empno, phone, day, adress, gender, birthday, level, email);
	}

	public void search() {
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			conn = DB.oraConn();
			StringBuilder sb = new StringBuilder();
			sb.append("select l.id, l.empno,l.email,l.phone,");
			sb.append("l.name,l.lv,l.days,");
			sb.append("l.address1, l.gender, l.birthday");
			sb.append(" from login l");
			sb.append(" where l.id=?");
			
			pstmt = conn.prepareStatement(sb.toString());
			String id = tfId.getText();
			pstmt.setString(1, id);
			rs = pstmt.executeQuery();
			dto = dao.detailMember(id);
			if (dto != null) {
				tfId.setText(id + "");
				tfName.setText(dto.getName());
				tfEmail.setText(dto.getEmail());
				tfDays.setText(String.valueOf(dto.getDays()));
				tfPhone.setText(dto.getPhone());
				if (dto.getGender().equals("남성")) {
					rdbtnMan.setSelected(true);
					rdbtnWoman.setSelected(false);
				} else if (dto.getGender().equals("여성")){
					rdbtnMan.setSelected(false);
					rdbtnWoman.setSelected(true);
				} else {
					buttonGroup.clearSelection();
					JOptionPane.showMessageDialog(MemberManage.this, "성별을 선택하세요.");
				}
				comboLevel.setSelectedItem(dto.getLv());
				tfAddress.setText(dto.getAddress1());
				tfBirth.setText(String.valueOf(dto.getBirthday()));
				tfEmpno.setText(String.valueOf(dto.getEmpno()));
			} else if (dto == null) {
				JOptionPane.showMessageDialog(MemberManage.this, "아이디가 없습니다.");
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (pstmt != null)
					pstmt.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			try {
				if (conn != null)
					conn.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	public void clear() {
		tfId.setText("");
		tfName.setText("");
		tfEmail.setText("");
		tfDays.setText("");
		tfPhone.setText("");
		tfBirth.setText("");
		tfAddress.setText("");
		tfEmpno.setText("");
		buttonGroup.clearSelection();
		comboLevel.setSelectedItem("");
	}
}
