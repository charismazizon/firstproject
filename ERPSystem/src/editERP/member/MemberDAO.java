package editERP.member;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import editERP.DB;

public class MemberDAO {
	public MemberDTO detailMember2(int empno){
		MemberDTO dto=null;
		Connection conn=null;
		PreparedStatement pstmt=null;
		ResultSet rs=null;
		try {
			conn=DB.oraConn();
			StringBuilder sb=new StringBuilder();
			sb.append("select   l.id, NVL(l.empno, 0) as empno, NVL(l.email,'edit@edit') as email, NVL(l.phone,'edit') as phone, NVL(l.name, '수정') as name, ");
			sb.append("  l.lv, l.days, NVL(l.address1, '수정하세요') as address1, NVL(l.gender, '선택안함') as gender, NVL(l.birthday, (select sysdate from dual)) as birthday ");
			sb.append(" from login l ");
			sb.append(" where empno=? ");
		
			
			pstmt = conn.prepareStatement(sb.toString());
			pstmt.setInt(1, empno);
			rs=pstmt.executeQuery();
			if(rs.next()){
				dto=new MemberDTO();
				dto.setAddress1(rs.getString("address1"));
				dto.setDays(rs.getDate("days"));
				dto.setBirthday(rs.getDate("birthday"));
				dto.setEmail(rs.getString("email"));
				dto.setEmpno(rs.getInt("empno"));
				dto.setGender(rs.getString("gender"));
				
				dto.setId(rs.getString("id"));
				dto.setLv(rs.getString("lv"));
				dto.setName(rs.getString("name"));
				dto.setPhone(rs.getString("phone"));
				
			}
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			try {
				if(rs!=null)rs.close();
				
			} catch (SQLException e2) {
				e2.printStackTrace();
			}
	try {
		if(pstmt!=null)pstmt.close();
				
			} catch (SQLException e2) {
				e2.printStackTrace();
			}
	try {
		
		if(conn!=null)conn.close();
	} catch (SQLException e2) {
		e2.printStackTrace();
	}
		}
	return dto;
	}
	
	public ArrayList<String> listMember() {
		ArrayList<String> items=new ArrayList<>();
		Connection conn=null;
		PreparedStatement pstmt=null;
		ResultSet rs=null;
		try {
			conn=DB.oraConn();
			String sql="select (empno ||' '|| name) name from login order by empno";
			pstmt=conn.prepareStatement(sql);
			rs=pstmt.executeQuery();
			while(rs.next()) {
				items.add(rs.getString("name"));
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if(rs!=null) rs.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			try {
				if(pstmt!=null) pstmt.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			try {
				if(conn!=null) conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return items;
	}
	public  int insertMember(MemberDTO dto){
		int result=0;
		Connection conn=null;
		PreparedStatement pstmt=null;
		try {
			conn=DB.oraConn();
			String sql="insert into member1 values(?,?,?,?,?,?,?,?,?,?)";
			pstmt=conn.prepareStatement(sql);
			pstmt.setString(1, dto.getId());
			pstmt.setString(2, dto.getName());
			pstmt.setInt(3, dto.getEmpno());
			pstmt.setString(4, dto.getPhone());
			pstmt.setDate(5, dto.getDays());
			pstmt.setString(6, dto.getAddress1());
			pstmt.setString(7, dto.getGender());
			pstmt.setDate(8, dto.getBirthday());
			pstmt.setString(9, dto.getLv());
			pstmt.setString(10, dto.getEmail());
			
			result=pstmt.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			try {
				if(pstmt!=null)pstmt.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			try {
				if(conn!=null)conn.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return result;
	}
	public MemberDTO detailMember(String id){
		MemberDTO dto=null;
		Connection conn=null;
		PreparedStatement pstmt=null;
		ResultSet rs=null;
		try {
			conn=DB.oraConn();
			StringBuilder sb=new StringBuilder();
//			sb.append("select l.id, l.empno,l.email,l.phone,");
//			sb.append("l.name,l.lv,l.days,");
//			sb.append("l.address1, l.gender, l.birthday");
//			sb.append(" from login l");
//			sb.append(" where l.id=?");
			sb.append("select   l.id, NVL(l.empno, 0) as empno, NVL(l.email,'edit@edit') as email, NVL(l.phone,'edit') as phone, NVL(l.name, '수정') as name, ");
			sb.append("  l.lv, l.days, NVL(l.address1, '수정하세요') as address1, NVL(l.gender, '선택안함') as gender, NVL(l.birthday, (select sysdate from dual)) as birthday ");
			sb.append(" from login l ");
			sb.append(" where l.id=? ");
		
			
			pstmt = conn.prepareStatement(sb.toString());
			pstmt.setString(1, id);
			rs=pstmt.executeQuery();
			if(rs.next()){
				dto=new MemberDTO();
				dto.setAddress1(rs.getString("address1"));
				dto.setDays(rs.getDate("days"));
				dto.setBirthday(rs.getDate("birthday"));
				dto.setEmail(rs.getString("email"));
				dto.setEmpno(rs.getInt("empno"));
				dto.setGender(rs.getString("gender"));
				
				dto.setId(rs.getString("id"));
				dto.setLv(rs.getString("lv"));
				dto.setName(rs.getString("name"));
				dto.setPhone(rs.getString("phone"));
				
			}
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			try {
				if(rs!=null)rs.close();
				
			} catch (SQLException e2) {
				e2.printStackTrace();
			}
	try {
		if(pstmt!=null)pstmt.close();
				
			} catch (SQLException e2) {
				e2.printStackTrace();
			}
	try {
		
		if(conn!=null)conn.close();
	} catch (SQLException e2) {
		e2.printStackTrace();
	}
		}
	return dto;
	}
	
	public int deleteMember(String id){
		int result=0;
		Connection conn=null;
		PreparedStatement pstmt=null;
		try {
			conn=DB.oraConn();
			String sql="delete from login where id=?";
			pstmt=conn.prepareStatement(sql);
			pstmt.setString(1, id);
			result=pstmt.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			try {
				if(pstmt!=null)pstmt.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			try {
				if(conn!=null)conn.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return result;
	}
	
	public int updateMember(MemberDTO dto){
		int result=0;
		Connection conn=null;
		PreparedStatement pstmt=null;
		try {
			conn=DB.oraConn();
			String sql="update login"
					+" set name=?,empno=?,phone=?,days=?,address1=?,gender=?,birthday=?,lv=?,email=?"
					+" where id=? ";
			pstmt=conn.prepareStatement(sql);
			pstmt.setString(10, dto.getId());
			pstmt.setString(1, dto.getName());
			pstmt.setInt(2, dto.getEmpno());
			pstmt.setString(3, dto.getPhone());
			pstmt.setDate(4, dto.getDays());
			pstmt.setString(5, dto.getAddress1());
			pstmt.setString(6, dto.getGender());
			pstmt.setDate(7, dto.getBirthday());
			pstmt.setString(8, dto.getLv());
			pstmt.setString(9, dto.getEmail());
			
			result=pstmt.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			try {
				if(pstmt!=null)pstmt.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			try {
				if(conn!=null)conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return result;
	}
	

}
