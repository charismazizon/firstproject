package editERP.member;

import java.sql.Date;

public class MemberDTO {
	private String id;
	private String name;
	private int empno;
	private String phone;
	private Date days;
	private String address1;
	private String gender;
	private Date birthday;
	private String lv;
	private String email;
	
	public MemberDTO(){
		
	}
	
	

	public MemberDTO(String id, String name, int empno, String phone, Date days, String address1, String gender,
			Date birthday, String lv, String email) {
		super();
		this.id = id;
		this.name = name;
		this.empno = empno;
		this.phone = phone;
		this.days = days;
		this.address1 = address1;
		this.gender = gender;
		this.birthday = birthday;
		this.lv = lv;
		this.email = email;
	}



	public String getLv() {
		return lv;
	}

	public void setLv(String lv) {
		this.lv = lv;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getEmpno() {
		return empno;
	}
	public void setEmpno(int empno) {
		this.empno = empno;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public Date getDays() {
		return days;
	}
	public void setDays(Date days) {
		this.days = days;
	}
	public String getAddress1() {
		return address1;
	}
	public void setAddress1(String address1) {
		this.address1 = address1;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public Date getBirthday() {
		return birthday;
	}
	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}
	@Override
	public String toString() {
		return "MemberDTO [id=" + id + ", name=" + name + ", empno=" + empno + ", phone=" + phone + ", days=" + days
				+ ", address1=" + address1 + ", gender=" + gender + ", birthday=" + birthday 
				+ ", lv=" + lv + ", email=" + email + "]";
	}

	

}
