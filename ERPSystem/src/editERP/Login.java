package editERP;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import editERP.DB;
import editERP.Main;
import editERP.login.LoginIdFind;
import editERP.login.LoginPwdFind;
import editERP.login.LoginSave;

public class Login extends JFrame {

	private JPanel contentPane;
	private JLabel lblBackImg;
	private Image backImg;
	private Color color;
	private JLabel lblId;
	private JLabel lblPw;
	private JPasswordField tfPw;
	private JButton btnLogin;
	private JButton btnFindId;
	private JButton btnFindPw;
	private JTextField tfId;
	private String lv;
	private String name;
	private Main frm;

	/**
	 * Launch the application.
	 */

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Login frame = new Login();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Login(Main frm) {
		this();
		this.frm = frm;
	}

	public Login() {
		setTitle("카리스마zㅣ존전설z카페");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 452, 553);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		color = new Color(100, 100, 200, 100);

		lblId = new JLabel("ID : ");
		lblId.setFont(new Font("맑은 고딕", Font.PLAIN, 11));
		lblId.setBounds(100, 390, 30, 15);
		contentPane.add(lblId);

		lblPw = new JLabel("PW : ");
		lblPw.setFont(new Font("맑은 고딕", Font.PLAIN, 11));
		lblPw.setBounds(100, 417, 36, 15);
		contentPane.add(lblPw);

		tfId = new JTextField();
		tfId.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (tfId.getText().equals("")) {
					JOptionPane.showMessageDialog(Login.this, "아이디를 입력해주세요.");
					tfId.requestFocus();
				} else if (String.valueOf(tfPw.getPassword()).equals("")) {
					JOptionPane.showMessageDialog(Login.this, "암호를 입력해주세요.");
					tfPw.requestFocus();
				} else {
					mainLogin();
				}
			}
		});
		tfId.setFont(new Font("맑은 고딕", Font.PLAIN, 12));
		tfId.setBounds(161, 386, 116, 21);
		contentPane.add(tfId);
		tfId.setColumns(10);

		tfPw = new JPasswordField();
		tfPw.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (tfId.getText().equals("")) {
					JOptionPane.showMessageDialog(Login.this, "아이디를 입력해주세요.");
					tfId.requestFocus();
				} else if (String.valueOf(tfPw.getPassword()).equals("")) {
					JOptionPane.showMessageDialog(Login.this, "암호를 입력해주세요.");
					tfPw.requestFocus();
				} else {
					mainLogin();
				}
			}
		});
		
		
		tfPw.setFont(new Font("맑은 고딕", Font.PLAIN, 12));
		tfPw.setBounds(161, 413, 116, 21);
		contentPane.add(tfPw);

		JButton btnJoin = new JButton("회원가입");
		btnJoin.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				LoginSave main = new LoginSave();
				main.setVisible(true);
				main.setLocation(100, 300);
			}
		});
		btnJoin.setFont(new Font("맑은 고딕", Font.PLAIN, 11));
		btnJoin.setBounds(100, 444, 80, 23);
		contentPane.add(btnJoin);

		btnLogin = new JButton("로그인");
		btnLogin.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				mainLogin();
			}
		});

		btnLogin.setFont(new Font("맑은 고딕", Font.PLAIN, 11));
		btnLogin.setBounds(197, 444, 80, 23);
		contentPane.add(btnLogin);

		btnFindId = new JButton("ID 찾기");
		btnFindId.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				LoginIdFind main = new LoginIdFind();
				main.setVisible(true);
				main.setLocation(100, 300);
			}
		});
		btnFindId.setFont(new Font("맑은 고딕", Font.PLAIN, 11));
		btnFindId.setBounds(100, 471, 80, 23);
		contentPane.add(btnFindId);

		btnFindPw = new JButton("PW 찾기");
		btnFindPw.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				LoginPwdFind main = new LoginPwdFind();
				main.setVisible(true);
				main.setLocation(100, 300);

			}
		});
		btnFindPw.setFont(new Font("맑은 고딕", Font.PLAIN, 11));
		btnFindPw.setBounds(197, 471, 80, 23);
		contentPane.add(btnFindPw);
		JLabel lblLoginForm = new JLabel("");
		lblLoginForm.setBackground(color);
		lblLoginForm.setBounds(0, 355, 434, 161);
		lblLoginForm.setOpaque(true);
		contentPane.add(lblLoginForm);

		lblBackImg = new JLabel("");
		lblBackImg.setBackground(Color.WHITE);
		lblBackImg.setBounds(0, 0, 434, 359);
		contentPane.add(lblBackImg);

		backImg = Toolkit.getDefaultToolkit().getImage(getClass().getResource("Coffee.jpg"));
		ImageIcon icon = new ImageIcon(backImg);
		Image imageSrc = icon.getImage();
		Image imageNew = imageSrc.getScaledInstance(500, 450, Image.SCALE_AREA_AVERAGING);
		icon = new ImageIcon(imageNew);
		lblBackImg.setIcon(icon);
	}

	public void mainLogin() {
		String id = tfId.getText();
		String password = String.valueOf(tfPw.getPassword());
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		try {
			con = DB.oraConn();
			String sql = "select * from login where id=? and password=?";
			pst = con.prepareStatement(sql);
			pst.setString(1, id);
			pst.setString(2, password);
			rs = pst.executeQuery();

			if (rs.next()) {
				lv = rs.getString("lv");
				name = rs.getString("name");
				if (lv.equals("관리자")) {
					JOptionPane.showMessageDialog(Login.this, "["+lv+"]"+name+"님 환영합니다.");
					Main main = new Main(lv, name);
					main.setVisible(true);
					dispose();
				} else {
					JOptionPane.showMessageDialog(Login.this, "["+lv+"]"+name+"님 환영합니다.");
					Main main = new Main(lv, name);
					main.setVisible(true);
					dispose();
				}
			} else {
				JOptionPane.showMessageDialog(Login.this, "로그인 정보가 틀렸습니다.");
				tfId.setText("");
				tfPw.setText("");
				tfId.requestFocus();
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			try {
				if (pst != null)
					pst.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			try {
				if (con != null)
					con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
}
