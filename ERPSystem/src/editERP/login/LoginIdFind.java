package editERP.login;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import editERP.DB;

public class LoginIdFind extends JFrame {

	private JPanel contentPane;
	private JTextField tfName;
	private JTextField tfPhone;
	private JTextField tfEmail;

	/**
	 * Launch the application.
	 */
//	public static void main(String[] args) {
//		EventQueue.invokeLater(new Runnable() {
//			public void run() {
//				try {
//					LoginIdFind frame = new LoginIdFind();
//					frame.setVisible(true);
//				} catch (Exception e) {
//					e.printStackTrace();
//				}
//			}
//		});
//	}

	/**
	 * Create the frame.
	 */
	public LoginIdFind() {
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 258, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel lblNewLabel = new JLabel("이름:");
		lblNewLabel.setBounds(14, 12, 62, 18);
		contentPane.add(lblNewLabel);

		tfName = new JTextField();
		tfName.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				idFind();
			}
		});
		tfName.setBounds(82, 9, 116, 24);
		contentPane.add(tfName);
		tfName.setColumns(10);

		JLabel lblNewLabel_1 = new JLabel("연락처:");
		lblNewLabel_1.setBounds(14, 53, 62, 18);
		contentPane.add(lblNewLabel_1);

		tfPhone = new JTextField();
		tfPhone.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				idFind();
			}
		});
		tfPhone.setBounds(82, 50, 116, 24);
		contentPane.add(tfPhone);
		tfPhone.setColumns(10);

		JLabel lblNewLabel_2 = new JLabel("이메일:");
		lblNewLabel_2.setBounds(14, 97, 62, 18);
		contentPane.add(lblNewLabel_2);

		tfEmail = new JTextField();
		tfEmail.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				idFind();
			}
		});
		tfEmail.setBounds(82, 97, 116, 24);
		contentPane.add(tfEmail);
		tfEmail.setColumns(10);

		JButton btnNewButton = new JButton("찾기");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				idFind();
			}
		});
		btnNewButton.setBounds(33, 162, 105, 27);
		contentPane.add(btnNewButton);
	}

	public void idFind() {

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String name = tfName.getText();
		String phone = tfPhone.getText();
		String email = tfEmail.getText();
		try {
			conn = DB.oraConn();
			String sql = "select * from login" + " where name=? and phone=? and email=?";
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, name);
			pstmt.setString(2, phone);
			pstmt.setString(3, email);
			rs = pstmt.executeQuery();
			if (rs.next()) {
				JOptionPane.showMessageDialog(LoginIdFind.this, "아이디는 " + rs.getString("id") + " 입니다");
				dispose();
			} else {
				if (name.equals("")) {
					JOptionPane.showMessageDialog(LoginIdFind.this, "아이디 입력.");
					tfName.requestFocus();
				} else if (phone.equals("")) {
					JOptionPane.showMessageDialog(LoginIdFind.this, "연락처 입력.");
					tfPhone.requestFocus();
				} else if (email.equals("")) {
					JOptionPane.showMessageDialog(LoginIdFind.this, "이메일 입력.");
					tfEmail.requestFocus();
				} else {
					JOptionPane.showMessageDialog(LoginIdFind.this, "아이디가 없습니다");
					tfName.requestFocus();
				}
			}
		} catch (Exception e2) {
			e2.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
			try {
				if (pstmt != null)
					pstmt.close();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
			try {
				if (conn != null)
					conn.close();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		}
	}
}
