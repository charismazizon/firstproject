package editERP.login;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import editERP.DB;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.awt.event.ActionEvent;

public class LoginPwdFind extends JFrame {

	private JPanel contentPane;
	private JTextField tfId;
	private JLabel lblNewLabel_1;
	private JTextField tfPhone;
	private JLabel lblNewLabel_2;
	private JTextField tfName;

	/**
	 * Launch the application.
	 */
//	public static void main(String[] args) {
//		EventQueue.invokeLater(new Runnable() {
//			public void run() {
//				try {
//					LoginPwdFind frame = new LoginPwdFind();
//					frame.setVisible(true);
//				} catch (Exception e) {
//					e.printStackTrace();
//				}
//			}
//		});
//	}

	/**
	 * Create the frame.
	 */
	public LoginPwdFind() {
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 248, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel lblNewLabel = new JLabel("아이디:");
		lblNewLabel.setBounds(14, 22, 62, 18);
		contentPane.add(lblNewLabel);

		tfId = new JTextField();
		tfId.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				pwFind();
			}
		});
		tfId.setBounds(77, 20, 116, 24);
		contentPane.add(tfId);
		tfId.setColumns(10);

		lblNewLabel_1 = new JLabel("연락처:");
		lblNewLabel_1.setBounds(14, 102, 62, 18);
		contentPane.add(lblNewLabel_1);

		tfPhone = new JTextField();
		tfPhone.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				pwFind();
			}
		});
		tfPhone.setBounds(77, 100, 116, 24);
		contentPane.add(tfPhone);
		tfPhone.setColumns(10);

		lblNewLabel_2 = new JLabel("이름:");
		lblNewLabel_2.setBounds(14, 62, 62, 18);
		contentPane.add(lblNewLabel_2);

		tfName = new JTextField();
		tfName.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				pwFind();
			}
		});
		tfName.setBounds(77, 60, 116, 24);
		contentPane.add(tfName);
		tfName.setColumns(10);

		JButton btnNewButton = new JButton("찾기");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				pwFind();
			}
		});
		btnNewButton.setBounds(41, 169, 105, 27);
		contentPane.add(btnNewButton);
	}
	
	public void pwFind() {

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String id = tfId.getText();
		String phone = tfPhone.getText();
		String name = tfName.getText();
		try {
			conn = DB.oraConn();
			String sql = "select * from login" + " where id=? and name=? and phone=? ";
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, id);
			pstmt.setString(2, name);
			pstmt.setString(3, phone);
			rs = pstmt.executeQuery();
			if (rs.next()) {
				JOptionPane.showMessageDialog(LoginPwdFind.this, "비밀번호는 " + rs.getString("password") + " 입니다");
				dispose();
			} else {
				if (id.equals("")){
					JOptionPane.showMessageDialog(LoginPwdFind.this, "아이디 입력.");
					tfId.requestFocus();
				}else if (name.equals("")){
					JOptionPane.showMessageDialog(LoginPwdFind.this, "이름 입력.");
					tfName.requestFocus();
				}else if (phone.equals("")){
					JOptionPane.showMessageDialog(LoginPwdFind.this, "연락처 입력.");
					tfPhone.requestFocus();
				}else{
					JOptionPane.showMessageDialog(LoginPwdFind.this, "정보가 일치하지 않습니다");
					tfId.requestFocus();
				}
			}
		} catch (Exception e2) {
			e2.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
			try {
				if (pstmt != null)
					pstmt.close();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
			try {
				if (conn != null)
					conn.close();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		}
	}
}
