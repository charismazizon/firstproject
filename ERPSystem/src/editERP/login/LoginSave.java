package editERP.login;

import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import editERP.DB;

public class LoginSave extends JFrame {

	private JPanel contentPane;
	private JTextField tfId;
	private JTextField tfEmail;
	private JTextField tfPhone;
	private JTextField tfName;
	private JPasswordField tfPwd;
	private Image backImg;

	/**
	 * Launch the application.
	 */
	// public static void main(String[] args) {
	// EventQueue.invokeLater(new Runnable() {
	// public void run() {
	// try {
	// LoginSave frame = new LoginSave();
	// frame.setVisible(true);
	// } catch (Exception e) {
	// e.printStackTrace();
	// }
	// }
	// });
	// }

	/**
	 * Create the frame.
	 */
	public LoginSave() {
		setTitle("카리스마zㅣ존전설z카페");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 244, 319);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel lblNewLabel = new JLabel("아이디:");
		lblNewLabel.setBounds(14, 12, 62, 18);
		contentPane.add(lblNewLabel);

		tfId = new JTextField();
		tfId.setBounds(81, 10, 116, 24);
		contentPane.add(tfId);
		tfId.setColumns(10);

		JLabel lblNewLabel_1 = new JLabel("비밀번호:");
		lblNewLabel_1.setBounds(14, 45, 62, 18);
		contentPane.add(lblNewLabel_1);

		JLabel lblNewLabel_2 = new JLabel("이메일:");
		lblNewLabel_2.setBounds(14, 81, 62, 18);
		contentPane.add(lblNewLabel_2);

		tfEmail = new JTextField();
		tfEmail.setBounds(81, 79, 116, 24);
		contentPane.add(tfEmail);
		tfEmail.setColumns(10);

		JLabel lblNewLabel_3 = new JLabel("연락처:");
		lblNewLabel_3.setBounds(14, 117, 62, 18);
		contentPane.add(lblNewLabel_3);

		tfPhone = new JTextField();
		tfPhone.setBounds(81, 115, 116, 24);
		contentPane.add(tfPhone);
		tfPhone.setColumns(10);

		JLabel lblNewLabel_4 = new JLabel("이름:");
		lblNewLabel_4.setBounds(14, 153, 62, 18);
		contentPane.add(lblNewLabel_4);

		tfName = new JTextField();
		tfName.setBounds(81, 151, 116, 24);
		contentPane.add(tfName);
		tfName.setColumns(10);

		JButton btnNewButton = new JButton("회원가입");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int result = 0;
				Connection con = null;
				PreparedStatement pst = null;
				String id = tfId.getText();
				String password = String.valueOf(tfPwd.getPassword());
				String email = tfEmail.getText();
				String phone = tfPhone.getText();
				String name = tfName.getText();
				try {
					con = DB.oraConn();
					String sql = "INSERT INTO login (empno, id, password, email, phone, name, days) values (loginplus.nextval, ?, ?, ?, ?, ?, (SELECT SYSDATE FROM DUAL))";
					pst = con.prepareStatement(sql);
					pst.setString(1, id);
					pst.setString(2, password);
					pst.setString(3, email);
					pst.setString(4, phone);
					pst.setString(5, name);
					result = pst.executeUpdate();
				} catch (Exception e2) {
					if (password.equals("")) {
						JOptionPane.showMessageDialog(LoginSave.this, "값. 입력.");
					} else {
						e2.printStackTrace();
					}
				} finally {
					try {
						if (pst != null) pst.close();
					} catch (SQLException e1) {
						e1.printStackTrace();
					}
					try {
						if (con != null) con.close();
					} catch (SQLException e1) {
						e1.printStackTrace();
					}
				}
				if (result == 1) {
					JOptionPane.showMessageDialog(LoginSave.this, "저장되었습니다");
					dispose();
				} else {
					JOptionPane.showMessageDialog(LoginSave.this, "실패.");
				}

			}
		});
		btnNewButton.setBounds(123, 207, 93, 27);
		contentPane.add(btnNewButton);

		JButton btnNewButton_1 = new JButton("중복체크");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Connection conn = null;
				PreparedStatement pstmt = null;
				ResultSet rs = null;
				String id = tfId.getText();
				try {
					conn = DB.oraConn();
					String sql = "select id from login where id=?";
					pstmt = conn.prepareStatement(sql);
					pstmt.setString(1, id);
					rs = pstmt.executeQuery();
					if (rs.next()) {
						JOptionPane.showMessageDialog(LoginSave.this, "중복.");
					} else if (id.equals("")) {
						JOptionPane.showMessageDialog(LoginSave.this, "빈값.");
					} else {
						JOptionPane.showMessageDialog(LoginSave.this, "사용가능.");
					}

				} catch (Exception e2) {
					e2.printStackTrace();
				} finally {
					try {
						if (rs != null) rs.close();
					} catch (SQLException e1) {
						e1.printStackTrace();
					}
					try {
						if (pstmt != null) pstmt.close();
					} catch (SQLException e1) {
						e1.printStackTrace();
					}
					try {
						if (conn != null) conn.close();
					} catch (SQLException e1) {
						e1.printStackTrace();
					}
				}

			}
		});
		btnNewButton_1.setBounds(10, 207, 93, 27);
		contentPane.add(btnNewButton_1);

		tfPwd = new JPasswordField();
		tfPwd.setBounds(81, 43, 116, 24);
		contentPane.add(tfPwd);

		JLabel lblBackImg = new JLabel("");
		lblBackImg.setBounds(0, 0, 322, 193);
		contentPane.add(lblBackImg);
		//
		// backImg =
		// Toolkit.getDefaultToolkit().getImage(getClass().getResource("Coffee.jpg"));
		// ImageIcon icon = new ImageIcon(backImg);
		// lblBackImg.setIcon(icon);
	}
}
