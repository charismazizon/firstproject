package editERP.notice;

import java.sql.Date;

public class NoticeDTO {
	private int num;
	private Date today;
	private String memlv;
	private String memname;
	private String noticetext;
	public int getNum() {
		return num;
	}
	public void setNum(int num) {
		this.num = num;
	}
	public Date getToday() {
		return today;
	}
	public void setToday(Date today) {
		this.today = today;
	}
	public String getMemlv() {
		return memlv;
	}
	public void setMemlv(String memlv) {
		this.memlv = memlv;
	}
	public String getMemname() {
		return memname;
	}
	public void setMemname(String memname) {
		this.memname = memname;
	}
	public String getNoticetext() {
		return noticetext;
	}
	public void setNoticetext(String noticetext) {
		this.noticetext = noticetext;
	}
	public NoticeDTO(int num, Date today, String memlv, String memname, String noticetext) {
		super();
		this.num = num;
		this.today = today;
		this.memlv = memlv;
		this.memname = memname;
		this.noticetext = noticetext;
	}
	@Override
	public String toString() {
		return "NoticeDTO [num=" + num + ", today=" + today + ", memlv=" + memlv + ", memname=" + memname
				+ ", noticetext=" + noticetext + "]";
	}
	
	
	 public NoticeDTO() {
		// TODO Auto-generated constructor stub
	}
}