package editERP.notice;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import editERP.DB;

public class NoticeText {

	public ArrayList<NoticeDTO> listNotice() {
		ArrayList<NoticeDTO> list = new ArrayList<>();
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		try {
			con = DB.oraConn();
			String sql = "SELECT num, today, memlv, memname, noticetext FROM notice ORDER BY num asc";
			pst = con.prepareStatement(sql);
			rs = pst.executeQuery();
			while(rs.next()){
				int num = rs.getInt("num");
				Date today = rs.getDate("today");
				String memlv = rs.getString("memlv");
				String memname = rs.getString("memname");
				String noticetext = rs.getString("noticetext");
				NoticeDTO dto = new NoticeDTO(num, today, memlv, memname, noticetext);
				list.add(dto);
			}
		
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (rs != null) rs.close();
			} catch (SQLException e2) {
				// TODO Auto-generated catch block
			}
			try {
				if(pst != null) pst.close();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
			try {
				if(con != null) con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return list;
	}	
	
	public int insertNotice(String memlv, String memname, String noticetext){
		int res = 0;
		Connection con = null;
		PreparedStatement pst = null;
		try {
			con = DB.oraConn();
			String sql = "INSERT into notice (num, today, memlv, memname, noticetext) values (numplus.nextval, sysdate, ?, ?, ?)";
			pst = con.prepareStatement(sql);
			pst.setString(1, memlv);
			pst.setString(2, memname);
			pst.setString(3, noticetext);
			res = pst.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			
				try {
					if(pst != null) pst.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			
				try {
					if(con != null) con.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
		}
		return res;
	}
	
	public int deleteNotice(int num){
		int res = 0;
		Connection con = null;
		PreparedStatement pst = null;
		try {
			con = DB.oraConn();
			String sql = "DELETE FROM notice WHERE num=?";
			pst = con.prepareStatement(sql);
			pst.setInt(1, num);
			res = pst.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (pst != null) pst.close();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
			try {
				if (con != null) con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return res;
	}
}
